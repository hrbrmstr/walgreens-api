Support code for the 2023-07-07 "Weekend Project Edition" of hrbrmstr's Daily Drop

```
├── 2023-07-06-stores.psv # data from one run on july 6, 2023
├── 2023-07-06-stores.txt # data from one run on july 6, 2023
├── README.md
├── golang
│   ├── justfile
│   ├── store-info.go     # get individual store info
│   └── store-list.go     # get the store list
├── rstats
│   └── store-stuff.R     # R code snippets (not as complete as the golang and rust ones)
└── rust
    ├── store-info        # get individual store info
    │   └── src
    │       └── main.rs
    └── store-list        # get individual store info
        ├── Cargo.toml
        └── src
            └── main.rs
```

![walgreens map](2023-07-06-conus-walgreens.png)