use std::collections::HashMap;
use std::env;
use reqwest::Client;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct Address {
	street: String,
	city: String,
	state: String,
	zip: String,
	county: String,
	#[serde(rename = "postalCode")]
	postal_code: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct StoreDetailsResponse {
	#[serde(rename = "storeNumber")]
	store_number: String,
	#[serde(rename = "storeType")]
	store_type: String,
	address: Address,
	latitude: String,
	longitude: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
	// Get the API key from environment variable
	let api_key = env::var("WALGREENS_API_KEY").expect("WALGREENS_API_KEY must be set");
	
	// Get the store number from the command line
	let args: Vec<String> = env::args().collect();
	if args.len() < 2 {
		println!("Usage: store-info store_number");
		return Ok(());
	}
	let store_number = &args[1];
	
	// Define the request body
	let mut map = HashMap::new();
	map.insert("apiKey", api_key);
	map.insert("affId", "Self".to_string());
	map.insert("storeNo", store_number.to_string());
	
	// Send POST request
	let client = Client::new();
	let res = client.post("https://services-qa.walgreens.com/api/stores/details/v1")
	.json(&map)
	.send()
	.await?;
	
	// Parse the response body
	let response_body: StoreDetailsResponse = res.json().await?;
	
	// Print store details (converting this to PSV is an exercise left to the reader)
	println!("{:?}", response_body);
	
	Ok(())
}
