use std::collections::HashMap;
use std::env;
use reqwest::Client;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct Response {
	#[serde(rename = "errDesc")]
	err_desc: String,
	#[serde(rename = "errCode")]
	err_code: String,
	store: Vec<String>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
	// Get the API key from environment variable
	let api_key = env::var("WALGREENS_API_KEY").expect("WALGREENS_API_KEY must be set");
	
	// Define the request body
	let mut map = HashMap::new();
	map.insert("apiKey", api_key);
	map.insert("affId", "Self".to_string());
	map.insert("act", "storenumber".to_string());
	
	// Send POST request
	let client = Client::new();
	let res = client.post("https://services-qa.walgreens.com/api/util/storenumber/v1")
	.json(&map)
	.send()
	.await?;
	
	// Parse the response body
	let response_body: Response = res.json().await?;
	
	// Print store numbers
	for store in response_body.store {
		println!("{}", store);
	}
	
	Ok(())
}
