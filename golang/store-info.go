package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type StoreInfoPayload struct {
	ApiKey  string `json:"apiKey"`
	AffId   string `json:"affId"`
	StoreNo string `json:"storeNo"`
}

type StoreInfoResponseData struct {
	// Add fields here as per your requirement
	StoreNumber string `json:"storeNumber"`
	Address     struct {
		Street string `json:"street"`
		City   string `json:"city"`
		State  string `json:"state"`
		Zip    string `json:"zip"`
		County string `json:"county"`
	} `json:"address"`
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: go run main.go [store number]")
		os.Exit(1)
	}
	storeNo := os.Args[1]

	data := StoreInfoPayload{
		ApiKey:  os.Getenv("WALGREENS_API_KEY"),
		AffId:   "Self",
		StoreNo: storeNo,
	}
	payloadBytes, err := json.Marshal(data)
	if err != nil {
		fmt.Printf("error while marshalling data: %v\n", err)
		return
	}
	body := bytes.NewReader(payloadBytes)

	// Use this in "production": https://services.walgreens.com/api/stores/details/v1
	req, err := http.NewRequest("POST", "https://services-qa.walgreens.com/api/stores/details/v1", body)
	if err != nil {
		fmt.Printf("error while creating request: %v\n", err)
		return
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("error while making request: %v\n", err)
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("error while reading response body: %v\n", err)
		return
	}

	var responseData StoreInfoResponseData
	err = json.Unmarshal(bodyBytes, &responseData)
	if err != nil {
		fmt.Printf("error while unmarshalling response: %v\n", err)
		return
	}

	fmt.Printf("%v|%v|%v|%v|%v|%v|%v|%v\n",
		responseData.StoreNumber, responseData.Address.Street, responseData.Address.City,
		responseData.Address.State, responseData.Address.Zip, responseData.Address.County,
		responseData.Latitude, responseData.Longitude)
}
