package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type Payload struct {
	ApiKey string `json:"apiKey"`
	AffId  string `json:"affId"`
	Act    string `json:"act"`
}

type ResponseData struct {
	ErrDesc string   `json:"errDesc"`
	ErrCode string   `json:"errCode"`
	Store   []string `json:"store"`
}

func main() {
	data := Payload{
		ApiKey: os.Getenv("WALGREENS_API_KEY"),
		AffId:  "Self",
		Act:    "storenumber",
	}
	payloadBytes, err := json.Marshal(data)
	if err != nil {
		fmt.Printf("error while marshalling data: %v\n", err)
		return
	}
	body := bytes.NewReader(payloadBytes)

	// Use this in "production": https://services.walgreens.com/api/util/storenumber/v1
	req, err := http.NewRequest("POST", "https://services-qa.walgreens.com/api/util/storenumber/v1", body)
	if err != nil {
		fmt.Printf("error while creating request: %v\n", err)
		return
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("error while making request: %v\n", err)
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("error while reading response body: %v\n", err)
		return
	}

	var responseData ResponseData
	err = json.Unmarshal(bodyBytes, &responseData)
	if err != nil {
		fmt.Printf("error while unmarshalling response: %v\n", err)
		return
	}

	for _, storeNo := range responseData.Store {
		fmt.Println(storeNo)
	}

}
